#!/usr/bin/groovy
package com.userheap;

def execute(){
    stage('deploy'){
        withCredentials([[$class: 'FileBinding', credentialsId: 'ssh-key', variable: 'SECRET_FILE']])
	{
		dir('ansible'){
			sh "ansible-playbook -i inv --key-file=$SECRET_FILE  playbook.yml"}
    	}
      }
}
return this;

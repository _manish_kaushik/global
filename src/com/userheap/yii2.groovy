#!/usr/bin/env groovy

package com.userheap;
def run(Object step, opts){
    try {
        if (opts) {
            step.execute(opts)
        } else {
            step.execute()
        }
     }  catch (err) {
          currentBuild.result = "FAILURE"
          echo "Build Failed"
     }
}

def execute(projectName) {
    this.run(new Checkout(), null);
    this.run(new ComposerInstall(), null);
    
    stage('test') {
	sh './vendor/bin/codecept  run';
	}

    if (env.BRANCH_NAME == "master"){
	this.run(new Confirm(),null);
	this.run(new BuildDeb(), [name: projectName, version: "1.0.${env.BUILD_NUMBER}"]);
	this.run(new Deploy(),null);
	} else {
	     echo "Not On Master"
	}
	}


return this;

